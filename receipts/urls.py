from django.urls import include, path

from receipts.views import (
    show_receipts_home,
    create_receipt,
    show_expense_categories,
    show_accounts,
    create_expense_category,create_account
)

urlpatterns = [
    path("", show_receipts_home, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", show_expense_categories, name="list_categories"),
    path("accounts/", show_accounts, name="list_accounts"),
    path(
        "categories/create/",
        create_expense_category,
        name="create_category",
    ),
    path(
        "accounts/create/",
        create_account,
        name="create_account",
    ),
]
